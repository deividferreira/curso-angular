import { Component, OnInit } from '@angular/core';
import { EstadosService } from '../estados.service';

@Component({
  selector: 'estados',
  templateUrl: './estados.component.html',
  styleUrls: ['./estados.component.css']
})
export class EstadosComponent implements OnInit {

  estados:any;

  constructor(private estadosService: EstadosService) { }

  ngOnInit(): void {
    this.estadosService.listaEstados().subscribe(
      response => {
        this.estados = response;
      }
    );
  }

}
