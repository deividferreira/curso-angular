import { Injectable } from '@angular/core';
import { Usuario } from './usuario';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  constructor() { }

  public getUsuario(): Usuario {
    let usuario = new Usuario();
    usuario.nome = "Deivid Ferreira";
    usuario.email = "deividpse@gmail.com";

    return usuario;
  }

  public listaUsuarios(): Usuario[] {
    return [
      {
        nome: "Deivid Ferreira",
        email: "deividpse@gmail.com"
      },
      {
        nome: "Maria de Jesus",
        email: "maria@gmail.com"
      },
      {
        nome: "João de Deus",
        email: "joaodedeus@gmail.com"
      },
      {
        nome: "Joaquim José",
        email: "jjose@gmail.com"
      }
    ]
  }
}
